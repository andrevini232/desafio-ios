//
//  RepositoryObject.swift
//  GetRepositry
//
//  Created by Andre on 02/07/17.
//  Copyright © 2017 Andre Vinicius. All rights reserved.
//

import UIKit
import ObjectMapper

class RepositoryPullObject: Mappable {

    
    var tituloPR : String?
    var bodyPR : String?
    var usrNamePR : String?
    var avatar_usr_url : String?
    var avatar : NSData?
    var user : NSDictionary?
    var repositoryName : NSString!
    var data : String?
    
    
    required init?(map: Map) {
        
    }

    func mapping(map: Map) {
        
        tituloPR <- map["title"]
        bodyPR <- map["body"]

        user <- map["user"]
        data <- map["updated_at"]
        usrNamePR = user?.object(forKey: "login") as? String
        avatar_usr_url = user?.object(forKey: "avatar_url") as? String
        
       data = formatData(data_pull: data!)
        
        setAvatarImage()
    }
    
    func formatData(data_pull : String) -> String{
        
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let date = dateFormatter.date(from: data_pull)!
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
        dateFormatter.locale = tempLocale // reset the locale
        let dateString = dateFormatter.string(from: date)
        
        return dateString
    }
     func setAvatarImage(){
        if avatar_usr_url != nil{
            let url = URL(string : avatar_usr_url!)
            let data = try? Data(contentsOf: url!)
            if data != nil {
                
                let image = UIImage(data: data!)
                
                self.avatar = UIImagePNGRepresentation(image!) as NSData!
            }
            
        }
    }
    
    
}

