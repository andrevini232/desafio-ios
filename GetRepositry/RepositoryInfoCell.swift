//
//  RepositoryInfoCell.swift
//  GetRepositry
//
//  Created by Andre on 02/07/17.
//  Copyright © 2017 Andre Vinicius. All rights reserved.
//
import UIKit

class RepositoryInfoCell: UITableViewCell {

    @IBOutlet weak var tituloPR: UILabel!
    
    @IBOutlet weak var bodyPR: UILabel!
    @IBOutlet weak var updatedLabel: UILabel!

    @IBOutlet weak var usrNameAutor: UILabel!
    @IBOutlet weak var imgAutor: UIImageView!
}
