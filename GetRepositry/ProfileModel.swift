//
//  User.swift

//  Created by Andre on 02/07/17.
//  Copyright © 2017 Andre Vinicius. All rights reserved.
//


import UIKit
import SystemConfiguration
import Alamofire
import ObjectMapper

import SwiftyJSON

class ProfileModel: NSObject {

    dynamic var flage : String!

    
    var allRepoObj : [RepositoryObject]! = []
    var allRepositoryPulls : [RepositoryPullObject]! = []
    
    override init(){
    
    
        super.init()
    }
    
    /**
     retorna pulls referentes ao repositorio especifico
     salva em cache os dados(datacore)
     */
    func getPullRequestData(repository : RepositoryObject, nameUsr : String, nameRepository : String){
        let str: String = "https://api.github.com/repos/\(nameRepository)/pulls"

        Alamofire.request(str)
            .responseJSON{
                response in
                if let json = response.result.value{
                    let jsondata = JSON(json)  
                    for item in jsondata{
                        let objc = Mapper<RepositoryPullObject>().map(JSONString : item.1.rawString()!)
                        objc?.repositoryName = nameRepository as NSString
                        self.allRepositoryPulls.append(objc!)
                       
                    }
                    self.flage = "pullData"
                    var repositoryNode = repository
                    repositoryNode.pulls = self.allRepositoryPulls
                    
                    //salvar informacoes em cache(coredata)
                    CoreData.storeTranscription(repositoryObject: repositoryNode, pulls: self.allRepositoryPulls)
                }
        }
    }
    
    /*
     Metodo para parsing das informacoes da primeira tela dos repositorios
     */
    func getUserData(){

//            /https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1
           let str: String = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1"
            
            Alamofire.request(str)
                
                .responseJSON{response in
                    if let json = response.result.value{
          
                        let jsondata = JSON(json)
                        let items = jsondata["items"]
                        
                        /*
                         cada repositorio = item.
                         construir a classe.
                         nome do repositorio, descricao do repositorio, nome/foto autor,
                         numero de stars, numero de forks
                         */
                        for item in items{

                            let objc = Mapper<RepositoryObject>().map(JSONString : item.1.rawString()!)
                            
                            self.allRepoObj.append(objc!)
                         }
                        self.flage = "goodData"
                    

                    }

            }
            
        
    }
        


    
}



/**
 
 extensao para verificar se esta on/off
 
 */

protocol Utilities {
}

extension NSObject:Utilities{
    
    
    enum ReachabilityStatus {
        case notReachable
        case reachableViaWWAN
        case reachableViaWiFi
    }
    
    var currentReachabilityStatus: ReachabilityStatus {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return .notReachable
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return .notReachable
        }
        
        if flags.contains(.reachable) == false {
            // The target host is not reachable.
            return .notReachable
        }
        else if flags.contains(.isWWAN) == true {
            // WWAN connections are OK if the calling application is using the CFNetwork APIs.
            return .reachableViaWWAN
        }
        else if flags.contains(.connectionRequired) == false {
            // If the target host is reachable and no connection is required then we'll assume that you're on Wi-Fi...
            return .reachableViaWiFi
        }
        else if (flags.contains(.connectionOnDemand) == true || flags.contains(.connectionOnTraffic) == true) && flags.contains(.interventionRequired) == false {
            // The connection is on-demand (or on-traffic) if the calling application is using the CFSocketStream or higher APIs and no [user] intervention is needed
            return .reachableViaWiFi
        }
        else {
            return .notReachable
        }
}
}

