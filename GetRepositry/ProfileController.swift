//
//  User.swift

//  Created by Andre on 02/07/17.
//  Copyright © 2017 Andre Vinicius. All rights reserved.
//

import UIKit

class ProfileController: NSObject,UITableViewDelegate,UITableViewDataSource{
    private var myContext = 0

    var myView : ProfileView!
    
    var myModel : ProfileModel!
    
    override init(){
    
    
        super.init()
        
        myModel = ProfileModel()
 
   
        
    }
    
    func addObserverModel(){
        myModel.addObserver(self,forKeyPath: "flage",options: .new,context: &myContext)

    }
    func removeObserverModel(){
        myModel.removeObserver(self, forKeyPath: "flage")
    }
    
    func callDAta(){
    
        myModel.getUserData()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if context == &myContext {
            
            
            if myModel.allRepoObj.count > 0 {
            
                myView.tableListView.reloadData()

            }
            
            
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return myModel.allRepoObj.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:RepositoryCell = tableView.dequeueReusableCell(withIdentifier: "CellID", for: indexPath) as! RepositoryCell
        
        //para cada usuario, mostra a lista com nome do repositorio e a linguagem relacionada
        cell.nameLabel.text = myModel.allRepoObj[indexPath.row].name
        cell.descricaoRepositorio.text = myModel.allRepoObj[indexPath.row].description
        cell.userName.text = myModel.allRepoObj[indexPath.row].login
       let img = UIImage(data: myModel.allRepoObj[indexPath.row].image! as Data)
        
            cell.imgUser.image = img
        cell.forkLabel.text = myModel.allRepoObj[indexPath.row].forks?.description
        cell.starLabel.text = myModel.allRepoObj[indexPath.row].stars?.description

        return cell
        
    }
    
    @objc func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let nextView = self.myView.storyboard?.instantiateViewController(withIdentifier: "repoDetailID") as! RepositoryDetailView!
        
        nextView?.userName = myModel.allRepoObj[indexPath.row].name
        nextView?.repositoryName =  myModel.allRepoObj[indexPath.row].name
        nextView?.currentRepository =  myModel.allRepoObj[indexPath.row]
        self.myView.navigationController?.pushViewController(nextView!, animated: true)
        
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

}


