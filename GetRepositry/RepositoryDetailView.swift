
    //  Created by Andre on 02/07/17.
    //  Copyright © 2017 Andre Vinicius. All rights reserved.
    //

/**
     
     Classe para detalhamento. 2a tela do repositorio especifico
     */
import UIKit

class RepositoryDetailView: UIViewController, UITableViewDataSource {
    @IBOutlet weak var repositoryNameLabel: UILabel!
    private var myContext = 0

    var pullTitle : String!
    var pullDescription : String!
    var userName : String!
    var repositoryName : String!
    var imgUser : UIImage!
    
    var repositoryModel : ProfileModel!
    var currentRepository : RepositoryObject!
    
    @IBOutlet weak var tableViewList: UITableView!
    
   
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if context == &myContext {
            
            
            if repositoryModel.allRepositoryPulls.count > 0 {
                
                tableViewList.reloadData()
              
            }
        }
    }
        
    override func viewDidLoad() {
    
       super.viewDidLoad()
        repositoryModel = ProfileModel()
        repositoryModel.getPullRequestData(repository: currentRepository, nameUsr: userName, nameRepository: repositoryName)
        
        tableViewList.dataSource = self
       
        repositoryModel.addObserver(self,forKeyPath: "flage",options: .new,context: &myContext)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        repositoryModel.removeObserver(self, forKeyPath: "flage")
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:RepositoryInfoCell = tableView.dequeueReusableCell(withIdentifier: "repCellID", for: indexPath) as! RepositoryInfoCell

        if repositoryModel.allRepositoryPulls.count > 0{
        cell.bodyPR.text = repositoryModel.allRepositoryPulls[indexPath.row].bodyPR
        cell.tituloPR.text = repositoryModel.allRepositoryPulls[indexPath.row].tituloPR
        cell.usrNameAutor.text = repositoryModel.allRepositoryPulls[indexPath.row].usrNamePR
        let img = UIImage(data: repositoryModel.allRepositoryPulls[indexPath.row].avatar! as Data)
        cell.imgAutor.image = img
        cell.updatedLabel.text = repositoryModel.allRepositoryPulls[indexPath.row].data
    }

        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return repositoryModel.allRepositoryPulls.count 
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    

}
