
//  Created by Andre on 02/07/17.
//  Copyright © 2017 Andre Vinicius. All rights reserved.
//
import UIKit

class RepositoryCell: UITableViewCell {

    
    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var descricaoRepositorio: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var forkLabel: UILabel!
    @IBOutlet weak var starLabel: UILabel!
}
