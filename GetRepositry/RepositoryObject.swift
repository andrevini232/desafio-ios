//
//  User.swift

//  Created by Andre on 02/07/17.
//  Copyright © 2017 Andre Vinicius. All rights reserved.
//

import UIKit
import ObjectMapper

//node para usuario/repositorio
struct RepositoryObject: Mappable {
    


    var id : Int!
    var name : String?
    var description : String?
    var login : String?
    var owner : NSDictionary?
    var forks : Int?
    var stars : Int?
    
    var avatar_usr_url : String?
    
    var image : NSData!

    var pulls : [RepositoryPullObject?] = []
    
    init(){
        
    }
    init?(map: Map) {
       
    }

    mutating func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["full_name"]
        description <- map["description"]
        owner <- map["owner"]
        login = owner?.object(forKey: "login") as? String
        forks <- map["forks"]
        stars <- map["stargazers_count"]
        
        avatar_usr_url = owner?.object(forKey: "avatar_url") as? String
        setAvatarImage()
    
    }
    
    
    mutating func setAvatarImage(){
        if avatar_usr_url != nil{
            let url = URL(string : avatar_usr_url!)
            let data = try? Data(contentsOf: url!)
                    if data != nil {
            
                            let image = UIImage(data: data!)
            
                            self.image = UIImagePNGRepresentation(image!) as NSData!
                    }

        }
    }

  
}
