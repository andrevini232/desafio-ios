//
//  User.swift

//  Created by Andre on 02/07/17.
//  Copyright © 2017 Andre Vinicius. All rights reserved.
//
/**
 
 Padrao DataAccessObject/DAO.
 Salva no cache
 Tabela relacional
 */
import UIKit

import CoreData
var CoreData : DataBaseModel! = DataBaseModel()
class DataBaseModel: NSObject {

    
  
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    
    /**
     Salvar os dados em cache
     relacao 1paran
     1 repositorio -> n pulls
     salva primeiro o repositorio, em seguida salva cada pull
     */
    func storeTranscription (repositoryObject : RepositoryObject, pulls : [RepositoryPullObject]) {
        let context = getContext()
       
        let entity =  NSEntityDescription.entity(forEntityName: "Repositry", in: context)
        
        let transc = NSManagedObject(entity: entity!, insertInto: context)
        
        transc.setValue(repositoryObject.id != nil ? repositoryObject.id : 0, forKey: "id")
        transc.setValue(repositoryObject.name != nil ? repositoryObject.name : "-", forKey: "name")
        transc.setValue(repositoryObject.login != nil ? repositoryObject.login : "-", forKey: "login")
        transc.setValue(repositoryObject.image != nil ? repositoryObject.image : "-", forKey: "imgrec")
        
        transc.setValue(repositoryObject.forks != nil ? repositoryObject.forks : "-", forKey: "forks")
        
        transc.setValue(repositoryObject.stars != nil ? repositoryObject.stars : "-", forKey: "stars")
        
        for item in pulls {
            

            let entityPull = NSEntityDescription.entity(forEntityName: "PullInfo", in: context)
            let newPull = NSManagedObject(entity: entityPull!, insertInto: context)
            
            /**
             
             */
            newPull.setValue(item.bodyPR != nil ? item.bodyPR : "-", forKey: "body")
            
            newPull.setValue(item.tituloPR != nil ? item.tituloPR : "-", forKey: "title")
            
            newPull.setValue(item.usrNamePR != nil ? item.usrNamePR : "-", forKey: "username")
            
            newPull.setValue(item.avatar != nil ? item.avatar : "-", forKey: "imageuser")
            
            
        
            transc.setValue(NSSet(object:newPull), forKey: "pulls")
            
            if transc != nil {
                //save the object
                do {
                    try context.save()
                    //print("saved!")
                }  catch {
                
                            }
            }
        
        }
        
        
    }
    
    
    
//    func getAllRepositories()-> [RepositoryObject]
//    {
//        var allRepoObject : [RepositoryObject]! = []
//        
//        let context = getContext()
//
//        
//        // Initialize Fetch Request
//        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
//        
//        
//        // Create Entity Description
//        let entityDescription = NSEntityDescription.entity(forEntityName: "Repositry", in: context)
//        
//        // Configure Fetch Request
//        fetchRequest.entity = entityDescription
//        
//        do {
//            let result = try context.fetch(fetchRequest) as! [Repositry]
//        
//            for item in result{
//                
//                var repository = RepositoryObject()
//                repository.id = Int(item.id)
//                repository.description = item.description
//                repository.forks = Int(item.forks)
//                repository.image = item.imgrec
//                repository.login = item.login
//                repository.name = item.name
//                
//                allRepoObject.append(repository)
//            
//                
//            }
//          
//            
//        } catch {
//            let fetchError = error as NSError
//       
//        }
//        
//       return allRepoObject
//     
//    }
    
    
    
    func deleteObject(name : String)
    {
        
        
        let context = getContext()
        
        
        // Initialize Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>()
        
        
        // Create Entity Description
        let entityDescription = NSEntityDescription.entity(forEntityName: "Repositry", in: context)
        
        
        
        let predicate = NSPredicate(format: "ownerName = %@", name)
        
        fetchRequest.predicate = predicate
        
        
        // Configure Fetch Request
        fetchRequest.entity = entityDescription
        
        do {
            let result = try context.fetch(fetchRequest) as! [Repositry]
            
            for item in result{
                
                
                context.delete(item)
                  
                    print("object deleted")
              
            
            }
            
            
            do {
                try context.save()
            }  catch {
                
            }
            
            
        } catch {
            _ = error as NSError
        }
       
        
        
        
    }
    
}
