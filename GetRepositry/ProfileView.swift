//
//  User.swift

//  Created by Andre on 02/07/17.
//  Copyright © 2017 Andre Vinicius. All rights reserved.
//
/*
 
 MVC aplicado aqui.
 
 Dividi a classe no modelo MVC:
 1. ProfileView para a referencia do conteudo da tela.
 2. ProfileController para as acoes.
 3. ProfileModel para json parsing e coredata
 4. classe databasemodel para o padrao DAO.
 5. node/classe UserObject para as informacoes do usuario e repositorios
*/

import UIKit

class ProfileView: UIViewController {

    
  
    @IBOutlet weak var tableListView: UITableView!
    
    var userName : String!

    var myController : ProfileController!
    
    @IBOutlet weak var usrImg: UIImageView!
    var repDetail : RepositoryDetailView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        myController = ProfileController()
        myController.myView = self
        
        tableListView.dataSource = myController
        tableListView.delegate = myController
        
        myController.callDAta()
        // Do any additional setup after loading the view.
    
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.myController.removeObserverModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.myController.addObserverModel()
    }


    
    
    
    

}
